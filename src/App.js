import React , { Component}  from 'react';
import './App.css';

function App() {

  function send(){
    let chats = localStorage.getItem("myChat");
    if(chats === null){
      let chats = [];
      let newChat = document.getElementById("chat").value;
      chats.push(newChat);
      localStorage.setItem("myChat", JSON.stringify(chats));
    } else {
      chats = JSON.parse(chats);
      let newChat = document.getElementById("chat").value;
      chats.push(newChat);
      localStorage.setItem("myChat", JSON.stringify(chats));
    }
  }

  function show(){
    let myChatInLocalStorage = localStorage.getItem("myChat");
    if(myChatInLocalStorage){
      myChatInLocalStorage = JSON.parse(myChatInLocalStorage);
      myChatInLocalStorage.map((chat, key) => {
        console.log("Chat " + key + ": " + chat);
      })
    } else {
      console.log("No chat in localStorage");
    }
  }

  return (
    <div className="App container">
      <div className="header">
        <h1 className="title">Local Storage Example</h1>
      </div>
      <div className="paragraph">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Faucibus ornare suspendisse sed nisi. Et ligula ullamcorper malesuada proin. Pulvinar proin gravida hendrerit lectus. Vel pharetra vel turpis nunc eget lorem dolor sed viverra. Vulputate mi sit amet mauris commodo quis. Nisl purus in mollis nunc sed id semper risus in. Nunc eget lorem dolor sed. Massa id neque aliquam vestibulum morbi. In dictum non consectetur a erat. Natoque penatibus et magnis dis parturient montes nascetur ridiculus. Non blandit massa enim nec dui nunc mattis enim ut. Convallis a cras semper auctor neque vitae tempus. Id semper risus in hendrerit gravida rutrum quisque non.</p>
        <p>Faucibus nisl tincidunt eget nullam non nisi. Bibendum neque egestas congue quisque egestas. Morbi tristique senectus et netus et. Et ligula ullamcorper malesuada proin libero nunc consequat. Vivamus at augue eget arcu dictum varius duis at. Adipiscing elit pellentesque habitant morbi. Mattis vulputate enim nulla aliquet porttitor. Id donec ultrices tincidunt arcu. Risus feugiat in ante metus. Ultrices dui sapien eget mi proin sed libero enim. Vulputate sapien nec sagittis aliquam. Urna neque viverra justo nec ultrices.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Faucibus ornare suspendisse sed nisi. Et ligula ullamcorper malesuada proin. Pulvinar proin gravida hendrerit lectus. Vel pharetra vel turpis nunc eget lorem dolor sed viverra. Vulputate mi sit amet mauris commodo quis. Nisl purus in mollis nunc sed id semper risus in. Nunc eget lorem dolor sed. Massa id neque aliquam vestibulum morbi. In dictum non consectetur a erat. Natoque penatibus et magnis dis parturient montes nascetur ridiculus. Non blandit massa enim nec dui nunc mattis enim ut. Convallis a cras semper auctor neque vitae tempus. Id semper risus in hendrerit gravida rutrum quisque non.</p>
        <p>Faucibus nisl tincidunt eget nullam non nisi. Bibendum neque egestas congue quisque egestas. Morbi tristique senectus et netus et. Et ligula ullamcorper malesuada proin libero nunc consequat. Vivamus at augue eget arcu dictum varius duis at. Adipiscing elit pellentesque habitant morbi. Mattis vulputate enim nulla aliquet porttitor. Id donec ultrices tincidunt arcu. Risus feugiat in ante metus. Ultrices dui sapien eget mi proin sed libero enim. Vulputate sapien nec sagittis aliquam. Urna neque viverra justo nec ultrices.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Faucibus ornare suspendisse sed nisi. Et ligula ullamcorper malesuada proin. Pulvinar proin gravida hendrerit lectus. Vel pharetra vel turpis nunc eget lorem dolor sed viverra. Vulputate mi sit amet mauris commodo quis. Nisl purus in mollis nunc sed id semper risus in. Nunc eget lorem dolor sed. Massa id neque aliquam vestibulum morbi. In dictum non consectetur a erat. Natoque penatibus et magnis dis parturient montes nascetur ridiculus. Non blandit massa enim nec dui nunc mattis enim ut. Convallis a cras semper auctor neque vitae tempus. Id semper risus in hendrerit gravida rutrum quisque non.</p>
      </div>
      <div className="chatbox">  
        <h2 className="chatbox-header">Chatbox</h2>
        <input type="text" id="chat" className="input"></input>
        <button onClick={send} className="btn btn-send">Send</button>  
        <br></br>      
        <button onClick={show} className="btn btn-show">Show messages</button>
      </div>
    </div>
  );
}

export default App;
